# 中国邮电学报Latex模板

[**README**](https://gitlab.com/xulongli/latex-style/-/blob/master/README.md)

中国邮电学报的Latex模板，官网只提供了[MS-DOC format](http://jcupt.bupt.edu.cn/download/E_lwmb.doc)格式的，以及[排版要求](https://jcupt.bupt.edu.cn/download/lwmb.doc).

##  参考资料
- [Elsevier authors instructions](https://www.elsevier.com/authors/author-schemas/latex-instructions)
- [Elsevier Camera-ready copy (CRC) journals latex template](https://www.elsevier.com/__data/assets/file/0003/56847/ecrc.sty)
- [JCUPT](https://jcupt.bupt.edu.cn/)
