# Latex template for JCUPT

[**中文说明**](https://gitlab.com/xulongli/latex-style/-/blob/master/README_Zh-CN.md)

Latex template for the Journal of China Universities of Posts and Telecommunications. The official template is in [MS-DOC format](http://jcupt.bupt.edu.cn/download/E_lwmb.doc).

Check the [author guide](https://jcupt.bupt.edu.cn/download/lwmb.doc) as well.

##  Refernce
- [Elsevier authors instructions](https://www.elsevier.com/authors/author-schemas/latex-instructions)
- [Elsevier Camera-ready copy (CRC) journals latex template](https://www.elsevier.com/__data/assets/file/0003/56847/ecrc.sty)
- [JCUPT Website](https://jcupt.bupt.edu.cn/)
